//
//  ViewController.swift
//  onepiece
//
//  Created by kai on 2021/06/13.
//

import UIKit

var charViewMode: CharacterViewModel = CharacterViewModel()

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charViewMode.getCount();
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToDetailVC" {
            if let VC = segue.destination as? DetailViewController {
                if let row = sender as? Int {
                    VC.viewModel.selectedChar = charViewMode.characterList[row]
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? viewcell else {
            return UITableViewCell()
        }
        
        cell.img.image = UIImage(named: "\(charViewMode.characterList(at: indexPath.row).name).jpg")
        cell.name.text = charViewMode.characterList[indexPath.row].name
        cell.price.text = String(charViewMode.characterList[indexPath.row].bounty)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "SegueToDetailVC", sender: indexPath.row)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}

class viewcell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
}
