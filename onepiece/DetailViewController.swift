//
//  DetailViewController.swift
//  onepiece
//
//  Created by kai on 2021/07/02.
//

import UIKit

class DetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UpdateUI()
    }
    
    func UpdateUI() {
        if let selectedChar = viewModel.selectedChar {
            img.image = UIImage(named: "\(selectedChar.name).jpg")
            print("selectedChar.name=\(selectedChar.name)")
            name.text = selectedChar.name
            price.text = String(selectedChar.bounty)
        }
    }
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    var viewModel = DetailViewModel()
}

class DetailViewModel {
    var selectedChar: Character?
    
    init() {
        selectedChar?.name = ""
        selectedChar?.bounty = 0
    }
}

