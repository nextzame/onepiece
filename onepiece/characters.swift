//
//  characters.swift
//  onepiece
//
//  Created by kai on 2021/06/27.
//

import Foundation

struct Character {
    var name: String
    var bounty: Int
    
    init(name: String, bounty: Int) {
        self.name = name
        self.bounty = bounty
    }
}

class CharacterViewModel {
    var characterList: [Character] = [
        Character(name: "brook", bounty: 3000),
        Character(name: "chopper", bounty: 50),
        Character(name: "franky", bounty: 440000),
        Character(name: "luffy", bounty: 50000000),
        Character(name: "nami", bounty: 600000),
        Character(name: "robin", bounty: 700000),
        Character(name: "sanji", bounty: 800000),
        Character(name: "zoro", bounty: 12000000),
    ]
    
    func getCount() -> Int {
        return characterList.count
    }
    
    func characterList(at index: Int) -> Character {
        return characterList[index]
    }
}
