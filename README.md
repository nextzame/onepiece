## 실행화면
### Screenshots
|||
|:--:|:--:|
|![화면_기록_2021-07-07_오전_6.24.06](/uploads/f9d9548f9e980a8438453527ef397623/화면_기록_2021-07-07_오전_6.24.06.gif)|실행화면입니다.|

## 사용된 기술
1. 화면 전환 - segue
2. tableView

## 구조
MVVM<br>
Model<-ModelView<->ViewController<->View
